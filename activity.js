/*db.users.insertMany([{
 "firstName": "Bill",
        "lastName": "Gates",
        "age": 65,
        "contact": {
                "phone": "09170123465",
                "email": "bill@mail.com"
            },
        "courses": ["PHP", "Laravel", "HTML"],
        "department": "HR"
},
{  
 "firstName": "Jane",
        "lastName": "Doe",
        "age": 21,
        "contact": {
                "phone": "09196543210",
                "email": "janedoe@mail.com"
            },
        "courses": ["CSS", "Javascript", "Python"],
        "department": "Operations"
},
{
        "firstName": "Stephen",
        "lastName": "Hawking",
        "age": 76,
        "contact": {
                "phone": "09191234567",
                "email": "stephenhawking@mail.com"
            },
        "courses": ["React", "PHP", "Python"],
        "department": "HR"
}, 
{
        "firstName": "Neil",
        "lastName": "Armstrong",
        "age": 82,
        "contact": {
                "phone": "09201234567",
                "email": "nielarmstrong@mail"
         },
        "courses": ["React", "Laravel", "SASS"],
        "department": "HR"
}]);*/

// ========================> #2
db.users.find({
    $or:
    [
    { "firstName":
        { $regex: "s", $options: '$i' }
    },
    { "lastName": 
        { $regex: "d", $options: '$i' }
    }
    ]},
    {
        "_id": 0,
        "firstName": 1,
        "lastName": 1
    }
);

// ========================> #3
db.users.find({
    $and: [
        {
            "department": "HR"
        },
        {
            "age": { $gte: 70}
        }
    ]
});

// =======================> #4
db.users.find({
    $and: [
        { "firstName":
            { $regex: "e", $options: '$i' }
        },
        {
            "age": { $lte: 30}
        }
    ]
});
